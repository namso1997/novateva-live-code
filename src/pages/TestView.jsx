import axios from "axios";
import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { response } from "../services/info";



const columns = [
    {
        name: 'Title',
        selector: row => row.name,
    },
    {
        name: 'Height',
        selector: row => row.height,
    },
    {
        name: 'Mass',
        selector: row => row.mass,
    },
    {
        name: 'Eye Color',
        selector: row => row.eye_color,
    },
    {
        name: 'Skin Color',
        selector: row => row.skin_color,
    }
];




const TestView = () => {


    const baseURL = 'https://swapi.dev/api'
    const [peopleMap, setPeopleMap] = useState(null)
    const [data, setData] = useState(null)
    // const [searchTerm, setSearchTerm] = useState('')
    // const [searchResults, setSearchResults] = useState('')

    // useEffect(() => {
    //     const objectfilter = products.filter( obj => {
    //     return ( obj.name.toLowerCase().includes(searchTerm) )
    //     } );
    //     setSearchResults(objectfilter);
    //   }, [searchTerm])
    const getPeople = () => {
        const [people, setPeople] = useState(null)

        useEffect(() => {
            axios.get(`${baseURL}/people`).then((response) => {
                setPeople(response.data);
            });

        }, []);
        return people
    }
    const getFilms = () => {
        const [films, setFilms] = useState(null)

        useEffect(() => {
            axios.get("https://swapi.dev/api/films/").then((response) => {
                setFilms(response.data);
            });

        }, []);
        return films
    }
    const films = getFilms()

    const people = getPeople()

    
    useEffect(() => {
        if (people != null) {

            setPeopleMap(people.results)

        } else {
            console.log('nada')
        }

    }, [people]);

    const ExpandedComponent = () => <pre>hola</pre>;






    return (
        <div className="bg-slate-200 h-screen flex  justify-center items-center">
            <div >
                {
                    peopleMap ?
                    <div>
                        
                        
                         <DataTable
                            columns={columns}
                            data={peopleMap}
                            expandableRows
                            expandableRowsComponent={ExpandedComponent}
                        />
                    </div>
                        :
                        <h1>Cargando</h1>
                }
            </div>

        </div>
    );
};

export default TestView;