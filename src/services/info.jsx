import axios from "axios";


export const fetchPeoplePromise = async () => {

    const baseURL = 'https://swapi.dev/api'
    const response = await axios.get(`${baseURL}/people/`);
    const { data } = response;

    return data.users

}
export const response = async ( ) => {
    const resp = await fetchPeoplePromise();

    return resp
}

