import { Outlet } from 'react-router-dom'

const Layout = () => {
    return (
        <div className="container md:flex md:min-h-screen">
         
            <main className="md:w-3/4 p-10 md:h-screen overflow-scroll">
             <Outlet/>
            </main>
        </div>
    );
};

export default Layout;